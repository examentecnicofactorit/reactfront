import api from './apiConfig';

export const getSpecialDate = () => api.get('/dates/getSpecialDate');