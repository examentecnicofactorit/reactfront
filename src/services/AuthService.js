import api from './apiConfig';
export const login = (email, password) => 
						api.post('/user/login',
							{ "email" :email,
									 "pass": password });