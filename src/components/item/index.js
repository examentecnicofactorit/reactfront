import React from 'react';

function Item({item, onAddItem, onRemoveItem}){
	const addItemToCart = () => onAddItem(item);
	const removeItemToCart = () => onRemoveItem(item.id);
	

	

	return (
		<div key={item.id}>
			<h2>{item.name}</h2>
			<span>$ {item.value}</span>
			<button onClick={addItemToCart}>+</button>
			<button onClick={ removeItemToCart}>-</button>
		</div>

	);
}
export	default Item;