import React,{useState} from 'react';
import { login } from '../../services/AuthService';

function Login({onLogin}) {
	const[email, setEmail] = useState('');
	const[password, setPassword] = useState('');
	const[error, setError] = useState('')


	const handleSubmit = (e) => {
		e.preventDefault();
		setError('');
		localStorage.clear();
		login(email , password).then(response =>{
			if(response.data.email !== undefined){
				localStorage.setItem('userEmail',response.data.email);
			    localStorage.setItem('userType', response.data.type);
			    onLogin()}
			else{
                setError('Usuario o Contraseña invalida');
			}
		})
	}
	
	return (
		<form onSubmit ={handleSubmit}>
			<div>
			<label for="email">Email</label>
			<input type = "email" onChange={(e) => setEmail(e.target.value)}/>
			</div>
			<br />
			<div>
			<label for="password">Password</label>
			<input type = "password"onChange={(e) => setPassword(e.target.value)}/>
			</div>
			<button type = "submit">LogIn</button> 
			{error ? <p>{error}</p> : null }
		</form>
	)
}


export default Login;