import React,{ useState, useEffect} from 'react';
import Item from './components/item';
import {getProducts} from  './services/ProductService';
import Login from './components/login';
import {getSpecialDate} from './services/SpecialDateService';




function App(){

	const [items, setItems] = useState([]);

	const [cart, setCart] = useState([]);

	const[user, setUserInfo] = useState(null);

	const [total, setTotal] = useState(0);

	const [totalProductos, setTotalProcutos] = useState(0)

	const [discount, setDiscount] = useState(0);

	const [specialDate, setDateInfo] = useState(null);

	const [cant, setCant] = useState([]);


	useEffect(() =>{
		localStorage.clear()
		refreshUserInfo();
		getSpecialDate().then(response => setDateInfo(response.data));
		if(user)getProducts().then(response => setItems(response.data));
		}
		

	, [] )

	const addItemToCart = (item) =>{
		const itemExist = cart.find(cartItem => cartItem.id === item.id)
		if(itemExist){
			const itemAEditar = cant.find(ob => ob.id === item.id) 
			const cantIdad = (itemAEditar.cantidad + 1)
			setCant(cant.pop(cantItem => cantItem.id === itemAEditar.id));
			setCant([...cant,{id: item.id, cantidad: cantIdad}])

		}else{
			setCart([...cart, item]);
			setCant([...cant,{id: item.id, cantidad: 1}])
			
		}
		setTotal(total + item.value);
		setTotalProcutos(totalProductos + 1)
		checkInDiscount();
	}

	const removeFromCart = (itemId) =>{
		const itemExist = cart.find(cartItem => cartItem.id === itemId)
		if(itemExist){
			const itemAEditar = cant.find(ob => ob.id === itemId) 
			if(itemAEditar !== undefined && itemAEditar.cantidad > 1){
				const cantIdad = (itemAEditar.cantidad - 1)
				setCant(cant.pop(cantItem => cantItem.id === itemAEditar.id));
				setCant([...cant,{id: itemId, cantidad: cantIdad}])
			}else if(itemAEditar !== undefined && itemAEditar.cantidad === 1){
				if(setCart(cart.filter(cartItem => cartItem.id !== itemId)));
			}
			if(itemExist) {
				setTotal(total - itemExist.value);
				setTotalProcutos(totalProductos - 1)
			}

		}

	}
	const refreshUserInfo = () => {
			if(localStorage.length > 0 && localStorage.getItem('userEmail') !== undefined){
				setUserInfo({
					email: localStorage.getItem('userEmail'),
					type: localStorage.getItem('userType')
				
			});
			getProducts().then(response => setItems(response.data))
		}
	}


	const checkInDiscount = () =>{
		//en caso de querer probar esta seccion hay que cambiar la fecha que esta seteada en la base o agregar la fecha del dia de la prueba
		if(user.type === '' && specialDate)setUserInfo({
			email: user.email,
			type: 'SPECIAL'
		})
		if(totalProductos === 3)setDiscount(total * 0.25)
		if(totalProductos > 8){
			switch (user.type){
				case 'VIP':
					setDiscount(500.0);
					break;
				case 'SPECIAL':
					setDiscount(300.0);
					break;
				default: 
					setDiscount(100.0);
			}
		}

		if(totalProductos > 3 && totalProductos <8) setDiscount(0)
	}

	const getCantItem = (item) =>{
		const itemToeval ={id: item.id, cantidad: 0}
		const cantReturn = cant.find(cantItem => cantItem.id === itemToeval.id)
			if(cantReturn)return(
				cantReturn.cantidad

			);
	}

	const clearCart = () =>{
		setCart([])
		setCant([])
		setTotal(0)
		setTotalProcutos(0)
		setDiscount(0)
	}


	return (
		<div>
			{user ? 
				
				<div>
				<h2>Listado de Items</h2>	
				{items.map(item => <Item item ={item} onAddItem ={addItemToCart} onRemoveItem={removeFromCart}
				/>)}
			</div>
			:
			<Login onLogin={refreshUserInfo}/>
			
			
			}
			{user && cart.length ?
			<div>
				<h2>Carrito</h2>
				{cart.map(cartItem => (
					<div key={cartItem.id}>
						{cartItem.name}  x {getCantItem(cartItem)}
						
					</div>
					
				))
				}
				<h3>Total de productos: {totalProductos}</h3>
                <h3>Descuento:{discount.toFixed(2)}</h3>
				<h3>Sub Total: {total.toFixed(2)}</h3>
				<h3>Total: {(total - discount).toFixed(2)} </h3>
				<button onClick={clearCart}>Limpiar Carro</button> 
			</div>
			: null
			}
			
		</div>
				

	);
}
export default App;

